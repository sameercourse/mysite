class CreatePortfolios < ActiveRecord::Migration[5.1]
  def change
    create_table :portfolios do |t|
      t.string :project
      t.text :description
      t.text :image

      t.timestamps
    end
  end
end
